from app import app
from flask import request,abort,jsonify,render_template,json
from app.models.table  import User,Product_Detail,Product_Bought,Product_Relationship,Recommondation
import datetime 

@app.route('/add/user',methods=['POST'])
def add_user():
    request_data=request.json
    name=request_data.get('name')
    if not name:
        abort(400,"name is not there")
    user=User()
    id=user.add_new_user(request_data)  
    result={"id":id}
    return jsonify(result)

@app.route('/add/product',methods=['POST'])
def add_product():
    request_data=request.json
    model=request_data.get('model')
    rating=request_data.get('rating')
    color=request_data.get('color')
    brand=request_data.get('brand')
    if not model:
        abort(400,"model is missing")
    if not rating:
        abort(400,"rating is missing")
    if not color:
        abort(400,"color is missing")
    if not brand:
        abort(400,"brand is missing")
    detail=Product_Detail()
    id=detail.add_new_product(request_data)
    result={"id":id}
    return jsonify(result)

@app.route('/bought/product',methods=['POST'])
def buying_bought():
    request_data=request.json
    user_id=request_data.get('user_id')
    product_id=request_data.get('product_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not product_id:
        abort(400,"product_id is missing")
    detail=Product_Bought()  
    id=detail.product_bought_detail(request_data)   
    if id:
        result={"status":"successfully bought"}
    return jsonify(result)   



@app.route('/suggested/products/<id>',methods=['GET'])
def get_products(id):
    re=Recommondation()
    products=re.get_all_products(id)
    result={'products':products}
    return  jsonify(result)





    


