from app import db
from datetime import datetime
from sqlalchemy import desc
import json

class User(db.Model):
    __tablename__="user"

    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(50),nullable=False)

    def add_new_user(self,data):
        try:
            user=User()
            user.name=data['name']
            db.session.add(user)
            db.session.flush()
            db.session.commit()
            return user.id
        except Exception as e:
            db.session.rollback()
            print e    
    

class Product_Detail(db.Model):
    __tablename__="product_detail"

    id=db.Column(db.Integer,primary_key=True)
    model=db.Column(db.String(30),nullable=False)
    rating=db.Column(db.Integer,nullable=False)
    color=db.Column(db.String(20),nullable=False)
    brand=db.Column(db.String(20),nullable=False)

    def add_new_product(self,data):
        try:
            product=Product_Detail()
            product.model=data['model']
            product.rating=data['rating']
            product.color=data['color']
            product.brand=data['brand']
            db.session.add(product)
            db.session.flush()
            db.session.commit()
            return product.id
        except Exception as e:
            db.session.rollback()
            print e    

    def serialize(self,id):
        result=db.session.query(Product_Detail).filter_by(id=id).first()
        return result
                

class Product_Bought(db.Model):
    __tablename__="product_bought"

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    product_id=db.Column(db.Integer,db.ForeignKey('product_detail.id'))
    bought_at=db.Column(db.DateTime, default=datetime.utcnow)

    def product_bought_detail(self,data):
        try:
            detail=Product_Bought()
            detail.user_id=data['user_id']
            detail.product_id=data['product_id']
            obj=db.session.query(Product_Bought).filter_by(user_id=detail.user_id).order_by(desc(Product_Bought.bought_at)).first()
            if obj:
                pr=Product_Relationship()
                pr.add_new_relation(obj.product_id,detail.product_id)

            db.session.add(detail)
            db.session.flush()
            db.session.commit()  
            return detail.id

        except Exception as e:
            db.session.rollback()
            print e    
   

class Product_Relationship(db.Model):
    __tablename__='product_relationship'

    id=db.Column(db.Integer,primary_key=True)
    product_id1=db.Column(db.Integer,nullable=False)
    product_id2=db.Column(db.Integer,nullable=False)
    count=db.Column(db.Integer,nullable=False)
    is_exist=db.Column(db.Integer,nullable=False)

    def add_new_relation(self,id1,id2):
        pr=Product_Relationship()
        pr.product_id1=id1
        pr.product_id2=id2
        obj=db.session.query(Product_Relationship).filter_by(product_id1=id1,product_id2=id2).first()
        if not obj:
            pr.count=1
            pr.is_exist=0
            db.session.add(pr)
            db.session.flush()
            db.session.commit()
        else:
            obj.count+=1
            db.session.add(obj)
            db.session.commit()

        value=db.session.query(Product_Relationship).filter_by(product_id1=id1,product_id2=id2).first()
        q=db.session.query(Recommondation).filter_by(product_id=value.product_id1).first()
        if not q:
            mn=Minheap()
            mn.insert(value)
            new_value=mn.printt()
            new_value=json.dumps(new_value)
            recommondation_product=Recommondation()
            recommondation_product.product_id=value.product_id1
            recommondation_product.products_array=new_value
            db.session.add(recommondation_product)
            db.session.flush()
            db.session.commit()
        else:
            if  q.product_id:
                array=q.products_array
                array=json.loads(array)
                mn=Minheap(array)
                mn.insert(value)
                new_value=mn.printt()
                new_value=json.dumps(new_value)
                q.products_array=new_value
                db.session.add(q)
                db.session.commit()


                

        




class Recommondation(db.Model):
    __tablename__='recommondation'

    id=db.Column(db.Integer,primary_key=True)
    product_id=db.Column(db.Integer,db.ForeignKey('product_detail.id'))
    products_array=db.Column(db.String(100),nullable=False)

    def get_all_products(self,id):
        product=db.session.query(Recommondation).filter_by(product_id=id).first()
        products=product.products_array
        products=json.loads(products)
        obj=Product_Detail()
        big_array=[]
        for i in range(0,len(products)):
            array=[]
            result=products[i][1]
            answer=obj.serialize(result)
            array.append([answer.id,answer.model,answer.rating,answer.color,answer.brand])
            big_array.append(array)    
        return big_array


class Minheap:
    def __init__(self,array=[]):
        self.heapsize=len(array)
        self.heap=array
    def parentup(self,i):
        return (i-1)/2
    def parentdown(self,i):
        return i-1/2     
    def leftchild(self,i):
        return 2*i+1
    def rightchild(self,i):
        return 2*i+2
    def insert(self,value):
        if value.is_exist==1:
            for i in range(0,self.heapsize):
                if self.heap[i][1]==value.product_id2:
                    self.heap[i][0]=self.heap[i][0]+1
                    break     
            self.heapifydown(0)        

        else:
            if self.heapsize<5:
                self.heap.append([value.count,value.product_id2])
                value.is_exist=1
                self.heapsize=self.heapsize+1
                self.heapifyup(self.heapsize-1)
            else:
                if self.heap[0][0]<value.count and self.heap[0][1]!=value.product_id2:
                    self.heap[0][0]=value.count
                    self.heap[0][1]=value.product_id2
                elif self.heap[0][0]<value.count and self.heap[0][1]==value.product_id2:
                    self.heap[0][0]=self.heap[0][0]+1




               
    def heapifyup(self,i):
        temp=self.heap[i][0]
        temp1=self.heap[i][1]
        while i>0 and temp<self.heap[self.parentup(i)][0]:
            self.heap[i][0]=self.heap[self.parentup(i)][0]
            self.heap[i][1]=self.heap[self.parentup(i)][1]
            i=self.parentup(i)
        self.heap[i][0]=temp 
        self.heap[i][1]=temp1   
    def printt(self):
        result=[]
        for i in range(self.heapsize):
            result.append(self.heap[i])
        return result   
    def delete(self,i):
        temp=self.heap[i]
        self.heap[i]=self.heap[self.heapsize-1]
        self.heapsize=self.heapsize-1
        self.heapifydown(i)
        return temp
    def heapifydown(self,i):
        parent=self.parentdown(i)
        left=self.leftchild(i)
        right=self.rightchild(i)
        if (2*i+2)<self.heapsize:
            if self.heap[left][0]<self.heap[parent][0] and self.heap[left][0]<self.heap[right][0]:
                temp=self.heap[left][0]
                temp1=self.heap[left][1]
                self.heap[left][0]=self.heap[parent][0]
                self.heap[left][1]=self.heap[parent][1]
                self.heap[parent][0]=temp
                self.heap[parent][1]=temp1
    
            elif  self.heap[right][0]<self.heap[parent][0] and self.heap[right][0]<self.heap[left][0]:
                temp=self.heap[right][0]
                temp1=self.heap[right][1]
                self.heap[right][0]=self.heap[parent][0]
                self.heap[right][1]=self.heap[parent][1]
                self.heap[parent][0]=temp
                self.heap[parent][1]=temp1
    
        elif (2*i+1)<self.heapsize:
            if self.heap[left][0]<self.heap[parent][0]:
                temp=self.heap[left][0]
                temp1=self.heap[left][1]
                self.heap[left][0]=self.heap[parent][0]
                self.heap[left][1]=self.heap[parent][1]
                self.heap[parent][0]=temp
                self.heap[parent][1]=temp1
    
    def getroot(self):
        return self.heap[0]
    def heapsize(self):
        return self.heapsize
        



    
    def getroot(self):
        return self.heap[0]
    def heapsize(self):
        return self.heapsize

