"""create account table

Revision ID: 1163fa906827
Revises: 
Create Date: 2017-03-14 15:01:31.603778

"""
from alembic import op
import sqlalchemy as sa
import datetime


# revision identifiers, used by Alembic.
revision = '1163fa906827'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False)
        )
    op.create_table(
        'product_detail',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('model', sa.String(30), nullable=False),
        sa.Column('rating', sa.Integer,nullable=False),
        sa.Column('color',sa.String(20),nullable=False),
        sa.Column('brand',sa.String(20),nullable=False)
        ) 
    op.create_table(
        'product_bought',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('product_id', sa.Integer, nullable=False),
        sa.Column('bought_at', sa.DateTime,default=datetime.datetime.utcnow)
        )
    op.create_table(
        'product_relationship',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('product_id1',sa.Integer,nullable=False),
        sa.Column('product_id2', sa.Integer, nullable=False),
        sa.Column('count', sa.Integer, nullable=False),
        sa.Column('is_exist', sa.Integer, nullable=False)
        )
    op.create_table(
        'recommondation',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('product_id',sa.Integer,nullable=False),
        sa.Column('products_array', sa.String(100), nullable=False)
        )
    op.create_foreign_key('user_key', 'product_bought','user', ['user_id'],['id'])
    op.create_foreign_key('product_key', 'product_bought','product_detail', ['product_id'],['id'])
    op.create_foreign_key('pro_key', 'recommondation','product_detail', ['product_id'],['id'])

def downgrade():
    op.drop_constraint('user_key', 'product_bought', type_='foreignkey')
    op.drop_constraint('product_key', 'product_bought', type_='foreignkey')
    op.drop_constraint('pro_key', 'recommondation', type_='foreignkey')
    op.drop_table('user')
    op.drop_table('product_detail')
    op.drop_table('product_bought')
    op.drop_table('product_relationship')
    op.drop_table('recommondation')
    